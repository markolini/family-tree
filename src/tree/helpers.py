from .node import Node


def map_family_nodes(family_pairs):
    relationships = {}
    for child_id, raw_parent_id in family_pairs.items():
        # striping \n character
        parent_id = raw_parent_id.strip()

        # checks if node is already present, else adds it now
        node = relationships.get(child_id)
        if node is None:
            node = Node(child_id)  # creates a new Node
            # adds new Node to family dict, using the child id as a key
            relationships[child_id] = node

        if child_id != parent_id:
            # sets a parent Node pointer if parent Node already exists
            parent = relationships.get(parent_id)
            if not parent:
                # creates a parent Node, if it is not already present
                parent = Node(parent_id)
                relationships[parent_id] = parent
            node.parent = parent

    return relationships


def print_tree(root, level=0):
    print("  " * level, root['id'])
    for child in root['children']:
        print_tree(child, level + 1)
