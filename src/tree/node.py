class Node(dict):

    def __init__(self, uid):
        self._parent = None  # pointer to parent Node
        self['id'] = uid  # keep reference to yourself
        self['children'] = []  # collection of pointers to children Nodes

    @property
    def parent(self):
        return self._parent

    @parent.setter
    def parent(self, node):
        self._parent = node
        # also add yourself as a child to the parent node
        node['children'].append(self)
