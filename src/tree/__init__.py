from .helpers import map_family_nodes, print_tree

def run_app():
    child_parent_pairs = {}
    with open('input', 'r') as file:
        for row in file:
            id, parent = row.split(' ')
            child_parent_pairs[id] = parent

    # lookup of all existing nodes with their children and parents
    nodes = map_family_nodes(child_parent_pairs)

    # scooping only root nodes
    roots = [node for node in nodes.values() if node.parent is None]

    for root in roots:
        print_tree(root)